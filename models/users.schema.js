const mongoose = require('mongoose');
const Cryptr   = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
const Schema = mongoose.Schema;


let userschema = new Schema({
    pseudo : String,
    email : String,
    password : String,
});

userschema.methods.generateHash = function(password) {
    return cryptr.encrypt(password)
};


userschema.methods.validPassword = function(password) {
    let pass1 = password;
    let pass2 = cryptr.decrypt(this.password);
    if(pass1 == pass2){
        return true;
    }
    return false;
};


let User = mongoose.model('User',userschema);

module.exports = User;
