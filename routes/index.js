const findUser = require('../config/find')

module.exports = function(app,passport){

    
    app.get('/',isLoggedIn,(req,res)=>{
        console.log("req user",req.user);
        const user = req.user;
        res.render('index',{
            user : req.user
        });
    });

    app.get('/login',(req,res) => {
        res.render('login')
    });

    app.post('/login',passport.authenticate('local-login',{
            successRedirect : '/',
            failureRedirect : '/login',
            failureFlash: true
        }
    ));

    app.get('/signup',(req,res) => {
        res.render('signup');
    })

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/',
        failureRedirect : '/signup',
        failureFlash : true 
    }));

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
       

    app.get('/reset',(req,res)=>{
        res.render('reset')
    })

    app.post('/reset',(req,res)=>{
        let email = req.body.email;
        const userf = findUser(email);
        res.redirect('/')
    })

    app.get('/membre',isLoggedIn,(req,res)=>{
        res.render('membre');
    })


    function isLoggedIn(req, res, next) {

        
        if (req.isAuthenticated())
            return next();

        
        res.redirect('/login');
    }

}