const LocalStrategy = require('passport-local').Strategy;


let User = require('../models/users.schema');
let mailer = require('./mailer')

module.exports = function(passport){

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
        
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req , email, password, done) {


        process.nextTick(function() {

        User.findOne({ 'email' :  email }, function(err, user) {

            if (err)
                return done(err);

            if (user) {
                return done(null, false, req.flash('signupMessage', 'l\adresse mail et deja utiliser fdp <3.'));
            } else {


                var newUser = new User();

      
                newUser.pseudo = req.body.pseudo;
                newUser.email = email;
                newUser.password = newUser.generateHash(password);

                var mailObtion = {
                    from: 'underthewolf07@gmail.com',
                        to: email,
                        subject: 'Inscription sur Monopoly ',
                        text: ''
                };
                mailer.sendMail(mailObtion,(err,info)=>{
                    if(err){
                        console.log(err);
                    }
                    else{
                        console.log("mail send : ",info.response)
                    }
                })
                newUser.save(function(err) {
                    if (err){
                        throw err;
                    }
                    
                    return done(null, newUser);
                });
            }

        });    

        });

    }));

    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    },


    function(req, email, password, done) { 
        User.findOne({ 'email' :  email }, function(err, user) {
            if (err)
                return done(err);
            console.log("user",user);
            console.log("password",password);
            if (!user)
                return done(null, false, req.flash('loginMessage', 'user non trouve')); 

            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'mauvais mdp')); 

            // retourne user
            return done(null, user);
        });

    }));

}