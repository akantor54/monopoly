const express = require('express');
// const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const app = express();
const passport = require('passport');
const flash = require('connect-flash');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');





app.set('view engine','ejs');

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())
mongoose.set('useUnifiedTopology', true);
const MONGODB_URI ="mongodb+srv://akantor:monopoly05@cluster0.oapfv.mongodb.net/monopoly?retryWrites=true&w=majority"

app.use(express.static('public'))
mongoose.connect(MONGODB_URI,{ useNewUrlParser : true });


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('connected');
});

app.use(session({secret : 'wolfclan'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./config/passport')(passport);
require('./routes/index')(app,passport);

const PORT = 3000;

app.listen(PORT,() => {
    console.log(`le serveur est dispo sur le port : ${PORT}`);
})